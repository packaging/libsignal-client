[[_TOC_]]

# [libsignal](https://github.com/signalapp/libsignal) client libraries

## Scope

### Minimum glibc

* amd64: 2.17+ (using [zig](https://andrewkelley.me/post/zig-cc-powerful-drop-in-replacement-gcc-clang.html))
* armv7/arm64: 2.28+

### Supported architectures

* amd64
* arm64/aarch64
* armv7

## Download

```
curl -Lo libsignal_jni.so "https://gitlab.com/packaging/libsignal-client/-/jobs/artifacts/${version}/raw/libsignal-client/${architecture}/libsignal_jni.so?job=libsignal-client-${architecture}"
```

**Example:**

```
curl -Lo libsignal_jni.so "https://gitlab.com/packaging/libsignal-client/-/jobs/artifacts/v0.19.3/raw/libsignal-client/amd64/libsignal_jni.so?job=libsignal-client-amd64"
```

## Support

If you like what i'm doing, you can support me via [Paypal](https://paypal.me/morph027), [Ko-Fi](https://ko-fi.com/morph027) or [Patreon](https://www.patreon.com/morph027).
