#!/bin/bash

set -e

apt_opts="-qqy"
if [[ -n "${DEBUG}" ]]; then
    set -x
    apt_opts="-y"
fi

sed '/^NAME/d' /etc/os-release > /tmp/os-release
# shellcheck disable=SC1091
. /tmp/os-release
export DEBIAN_FRONTEND=noninteractive
architecture="${ARCH:-amd64}"
zig_version="${ZIG_VERSION:-0.13.0}"
zig_target="${ZIG_TARGET:-2.17}"

echo "deb http://archive.debian.org/debian ${VERSION_CODENAME}-backports main" > "/etc/apt/sources.list.d/${VERSION_CODENAME}-backports.list"

case "${architecture}" in
    "arm64")
        dpkg --add-architecture arm64
        crosslibc="libc6:arm64"
        ;;
    "armv7")
        dpkg --add-architecture armhf
        crosslibc="libc6:armhf"
        ;;
esac

apt-get $apt_opts update
# shellcheck disable=SC2086
apt-get $apt_opts install \
    ${crosslibc} \
    curl \
    git \
    binutils \
    file


setup_zig() {
    local zig_target_triple="${1}"
    # zig_target_triple="x86_64-linux-gnu.${zig_target}"
    # zig_target_triple="aarch64-linux-gnu.${zig_target}"
    # zig_target_triple="arm-linux-gnueabihf.${zig_target}"
    if [ ! -d "/tmp/zig" ]; then
        curl -sLfo /tmp/zig.tar.xz "https://ziglang.org/download/${zig_version}/zig-linux-x86_64-${zig_version}.tar.xz"
        mkdir -p /tmp/zig
        tar -C /tmp/zig -xf /tmp/zig.tar.xz --strip-components=1
    fi
    echo -e '#!/bin/sh\n/tmp/zig/zig cc -v $@ -target' "${zig_target_triple}" >/usr/local/bin/zcc
    echo -e '#!/bin/sh\n/tmp/zig/zig c++ -v $@ -target' "${zig_target_triple}" >/usr/local/bin/zxx
    chmod +x /usr/local/bin/zcc /usr/local/bin/zxx
    echo "==="
    zcc
    echo "==="
    export CC=zcc
    export CXX=zxx
    export ZIG_LOCAL_CACHE_DIR="${ZIG_LOCAL_CACHE_DIR:-/zig}"
    export ZIG_GLOBAL_CACHE_DIR="${ZIG_GLOBAL_CACHE_DIR:-/zig}"
    mkdir -p "${ZIG_LOCAL_CACHE_DIR}" "${ZIG_GLOBAL_CACHE_DIR}"
}

setup_ccache() {
    local arch="${1}"
    export CCACHE_DIR="${CCACHE_DIR:-/tmp/ccache}"
    export RUSTC_WRAPPER=/usr/bin/ccache
    mkdir -p "$CCACHE_DIR"
    apt-get $apt_opts install \
        "gcc-${arch}" \
        "g++-${arch}"
    export CC="${arch}-gcc"
    export CXX="${arch}-g++"
    export CPATH="/usr/${arch}/include"
}

setup_cargo() {
    local arch="${1}"
    export CARGO_BUILD_TARGET="${arch}"
    rustup target add "${CARGO_BUILD_TARGET}"
    _linker="${CARGO_BUILD_TARGET^^}"
    _linker="CARGO_TARGET_${_linker//-/_}_LINKER"
    declare -gx "$_linker"="${CC}"
}
case "${NAME}" in
    "libsignal-client")
        apt-get $apt_opts -t "${VERSION_CODENAME}-backports" install \
            ccache \
            clang \
            cmake \
            git \
            libprotobuf-dev \
            protobuf-compiler
        export CARGO_CACHE_DIR="${CARGO_CACHE_DIR:-/cargo}"
        mkdir -p "${CARGO_CACHE_DIR}"
        ln -sf "${CARGO_CACHE_DIR}"/{.package-cache,git,registry} "${CARGO_HOME}/"
        mkdir -p "${CARGO_CACHE_DIR}"/{git,registry}
        if [ ! -d /tmp/libsignal-client ]; then
            git clone -b "${CI_COMMIT_TAG}" https://github.com/signalapp/libsignal-client /tmp/libsignal-client
        fi
        cd /tmp/libsignal-client
        export RUSTFLAGS="-C link-arg=-s"
        case "${architecture}" in
        "amd64")
            setup_zig "x86_64-linux-gnu.${zig_target}"
            setup_cargo x86_64-unknown-linux-gnu
            ;;
        "arm64")
            setup_ccache aarch64-linux-gnu
            setup_cargo aarch64-unknown-linux-gnu
            ;;
        "armv7")
            setup_ccache arm-linux-gnueabihf
            setup_cargo armv7-unknown-linux-gnueabihf
            ;;
        *)
            :
            ;;
        esac
        cargo test
        cargo \
            build \
            --release \
            --verbose \
            -p libsignal-jni
        echo "---"
        file "/tmp/libsignal-client/target/${CARGO_BUILD_TARGET}/release/libsignal_jni.so"
        echo "---"
        objdump -T "/tmp/libsignal-client/target/${CARGO_BUILD_TARGET}/release/libsignal_jni.so" | grep -o --color=never 'GLIBC.*' | sort -V
        echo "---"
        owner="$(stat -c %u "${CI_PROJECT_DIR}"/.gitlab-ci/build.sh)"
        group="$(stat -c %g "${CI_PROJECT_DIR}"/.gitlab-ci/build.sh)"
        install -d -o "${owner}" -g "${group}" "${CI_PROJECT_DIR}/libsignal-client/${architecture}"
        install \
            -o "${owner}" \
            -g "${group}" \
            "/tmp/libsignal-client/target/${CARGO_BUILD_TARGET}/release/libsignal_jni.so" \
            "${CI_PROJECT_DIR}/libsignal-client/${architecture}/"
        exit 0
        ;;
esac
