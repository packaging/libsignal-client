# DEV

## Build

### amd64

```bash
git clone https://gitlab.com/packaging/libsignal-client
cd libsignal-client
export ARCH=arm64
DOCKER_DEFAULT_PLATFORM="linux/${ARCH}" docker run --rm -it -e ARCH="${ARCH}" -e NAME=libsignal-client -e CI_COMMIT_TAG=v0.37.0 -e CI_PROJECT_DIR=/source -e CCACHE_DIR=/ccache -v $PWD:/source -v libsignal-client-cargo-cache:/cargo -v libsignal-client-ccache:/ccache --tmpfs /tmp/:exec -w /tmp rust:buster /source/.gitlab-ci/build.sh
```
